package com.kirill;

import java.util.OptionalInt;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
//        primitiveStream();
//        createStream();
//        generate();
//        iterate();
//        concat();
//        streamBuilder();
        range();
    }

    public static void primitiveStream() {
        IntStream intStream = IntStream.of(25, 10, 30, 55, 70, 90, 120);
        OptionalInt optional = intStream.filter(x -> x >= 30)
                .map(x -> x / 5)
                .min();
        if (optional.isPresent()) {
            System.out.println(optional.getAsInt());
        }
    }

    public static void createStream() {
        Stream.of(1, 2, 3, 4).forEach(System.out::println);
        Stream empty = Stream.empty();
//        String value = "Value";
        String value = null;
        Stream stream = Stream.ofNullable(value);
        stream.forEach(System.out::println);
    }

    public static void generate() {
        Supplier supplier = () -> new Random().nextInt(9);
        Stream.generate(supplier)
                .limit(10)
                .forEach(System.out::println);
    }

    public static void iterate() {
        Stream.iterate(1, x -> x < 30, x -> x * 2)
                .forEach(System.out::println);
    }

    public static void concat() {
        Stream first = Stream.of(1, 2, 3, 4, 5, 6);
        Stream second = Stream.of(7, 8, 9);
        Stream.concat(first, second).forEach(System.out::println);
    }

    public static void streamBuilder() {
        Stream.Builder<String> streamBuilder = Stream.builder();
        streamBuilder.accept("1");
        streamBuilder.add("2").add("3");
        streamBuilder.build();

    }

    public static void range() {
        IntStream.range(0, 5)
                .forEach(System.out::println);

        LongStream.rangeClosed(5, 10)
                .forEach(System.out::println);
    }

}
