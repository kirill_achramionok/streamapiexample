package com.kirill.samples;

import java.util.Random;

/**
 * @author kirill.akhramenok
 */
public class Entity {

    private String name;
    private String type;
    private int value;

    public Entity() {
        value = new Random().nextInt(7);
    }

    public Entity(String name, String type) {
        this.name = name;
        this.type = type;
        value = new Random().nextInt(7);
    }

    public String getKey() {
        return this.name + this.type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.type;
    }
}
