package com.kirill.samples;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author kirill.akhramenok
 */
public class Main {

    public static void main(String[] args) {
        List<Entity> entities = new ArrayList<>();
        entities.add(new Entity("1", "ABC"));
        entities.add(new Entity("2", "QWE"));
        entities.add(new Entity("3", "QWE"));
        entities.add(new Entity("4", "ABC"));
        entities.add(new Entity("5", "QWE"));
//
        filter(entities);
//        primitiveMapExample();
//        flatMap();
//        limit();
//        skip();
//        sorted();
//        distinct();
//        takeWhile();
//        dropWhile();
//        toMap(entities);
//        joining();
    }

    public static void filter(List<Entity> entityList) {
        for (Entity entity : entityList) {
            if (entity.getType().equals("QWE")) {
                System.out.println(entity);
            }
        }
        entityList.stream()
                .filter(x -> x.getType().equals("QWE"))
                .forEach(System.out::println);
    }

    public static void primitiveMapExample() {
        String[] strings = new String[]{"1", "2", "3", "4", "5", "-5", "-30", "40", "20"};
        for (String string : strings) {
            int value = Integer.parseInt(string);
            if (value > 0) {
                value += 12;
                System.out.println(value);
            }
        }
        System.out.println("____________________________________________________________________");
        Stream.of("1", "2", "3", "4", "5", "-5", "-30", "40", "20")
                .map(Integer::parseInt)
                .filter(x -> x > 0)
                .map(x -> x + 12)
                .forEach(System.out::println);
    }

    public static void flatMap() {
        int[] array = new int[]{1, 2, 3, 0};
        List<Entity> entities = new ArrayList<>();
        for (int i : array) {
            for (int j = 0; j < i; j++) {
                entities.add(new Entity());
            }
        }
        System.out.println(entities.size());
        System.out.println("________________________________________________________");
        List<Entity> entityList = Stream.of(1, 2, 3, 0)
                .flatMap(x -> Stream.generate(Entity::new).limit(x))
                .collect(Collectors.toList());
        System.out.println(entityList.size());
    }

    public static void limit() {
        int counter = 0;
        for (int i = 0; i < 5; i++) {
            new Entity();
            counter++;
        }
        System.out.println(counter);
        System.out.println(Stream.generate(Entity::new).limit(5)
                .count());
    }

    public static void skip() {
        Stream.of(1, 2, 3, 0)
                .skip(2)
                .forEach(System.out::println);
    }

    public static void sorted() {
        Stream.of(0, 9, 1, 8, 2, 7, 3, 6, 4, 5)
                .sorted()
                .forEach(System.out::println);
    }

    public static void distinct() {
        int[] array = new int[]{1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 8, 9};
        int[] distinct = new int[array.length];
        boolean isDuplicate = false;
        int k = 0;
        for (int i = 0; i < array.length; i++) {
            for (int value : distinct) {
                isDuplicate = false;
                if (array[i] == value) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                distinct[k] = array[i];
                k++;
            }
        }
        for (int i = 0; i < distinct.length; i++) {
            System.out.println(distinct[i]);
        }
        System.out.println("_______________________________________________________________");

        Stream.of(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 8, 9)
                .distinct()
                .forEach(System.out::println);
    }

    public static void takeWhile() {
        Stream.of(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 8, 9)
                .distinct()
                .takeWhile(x -> x < 5)
                .forEach(System.out::println);
    }

    public static void dropWhile() {
        Stream.of(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 8, 9)
                .distinct()
                .dropWhile(x -> x < 5)
                .forEach(System.out::println);
    }

    public static void collect(List<Entity> entityList) {
        List<Entity> entities = entityList.stream()
                .filter(x -> x.getType().equals("QWE"))
                .collect(Collectors.toList());
    }

    public static void toArray(List<Entity> entityList) {
        Entity[] entities = entityList.stream()
                .filter(x -> x.getType().equals("QWE"))
                .toArray(Entity[]::new);
    }

    public static void reduce() {

        int sum = Stream.of(1, 2, 3, 4, 5)
                .reduce(0, (acc, x) -> acc + x);
        System.out.println(sum);
        //min
        //max
        //Optional reduce
        //findAny​()
        // findFirst ()
        //allMatch
        //anyMatch
        //average  - only for primitive streams
        //sum
        //summaryStatistics
    }

    public static void toSet(List<Entity> entityList) {
        Set<Entity> entitySet = entityList.stream().collect(Collectors.toSet());
    }

    public static void toCollection(List<Entity> entityList) {
        HashSet<Entity> entitySet = entityList.stream().collect(Collectors.toCollection(HashSet::new));
        LinkedHashSet<Entity> linkedHashSet = entityList.stream().collect(Collectors.toCollection(LinkedHashSet::new));
        LinkedList<Entity> linkedList = entityList.stream().collect(Collectors.toCollection(LinkedList::new));
    }

    public static void toMap(List<Entity> entityList) {
        Map<String, Entity> entityMap = entityList.stream()
                .collect(Collectors.toMap(Entity::getKey, Function.identity(), (o, n) -> {
                    return n;
                }));
        entityMap.forEach((key, value) -> System.out.println("key: " + key + ", value: " + value));
    }

    public static void joining() {
        String str = Stream.of("1", "2", "3", "4")
                .collect(Collectors.joining(",", "prefix- ", " -suffix"));
        System.out.println(str);
    }
}
